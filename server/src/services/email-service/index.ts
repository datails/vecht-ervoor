import { Injectable, HttpService } from "@nestjs/common";
import * as sgMail from "@sendgrid/mail";
import * as functions from "firebase-functions";

@Injectable()
export class AppEmailService {
  private readonly config = functions.config().config
  private readonly blackList = [this.config.blacklist];

  constructor(private httpService: HttpService) {
    sgMail.setApiKey(this.config.smtp);
  }

  public async sendEmail(body: any, ip: string) {
    const validate = await this.httpService
      .post(
        `https://www.google.com/recaptcha/api/siteverify?response=${body.recaptchaValue}&secret=${this.config.recaptcha}&remoteip=${ip}`
      )
      .toPromise();

    if (!validate.data.success) {
      console.log("Failed recaptcha:", validate.data);
      return new Error("Recaptcha failed.");
    }

    if (this.blackList.includes(body?.email)) {
      return
    }

    await sgMail.send({
      to: 'info@vechtervoor.nl',
      from: "datails.smtp@gmail.com",
      subject: "Contact via de website",
      templateId: "d-bd2868734f274417a77d24515ea69d0b",
      dynamicTemplateData: {
        email: body.email,
        name: body.naam,
        body: body.message,
      },
    });
  }
}
