import "dotenv/config";

import * as functions from "firebase-functions";
import { NestFactory } from "@nestjs/core";
import { ExpressAdapter } from "@nestjs/platform-express";
import * as express from "express";
import { AppModule } from "./modules";

const server = express();
const isDev = process.env.NODE_ENV === "development";

async function bootstrapApp(server: express.Express) {
  // create nest app
  const app = await NestFactory.create(AppModule, new ExpressAdapter(server));

  if (!isDev) {
    app.enableCors({
      origin: ["https://vecht-ervoor.web.app", "https://vechtervoor.nl"],
    });
  }

  return app.init();
}

export const api = functions.https.onRequest(async (req, res) => {
  await bootstrapApp(server);
  server(req, res);
});
