import { Controller, Post, Body, HttpCode, Ip } from "@nestjs/common";
import { EmailDTO } from "./email.dto";
import { AppEmailService } from "../../services/email-service";

@Controller("send")
export class AppController {
  constructor(private readonly emailService: AppEmailService) {}
  @HttpCode(204)
  @Post()
  public async sendMailStichtingVechtErvoor(
    @Body() body: EmailDTO,
    @Ip() ip: string
  ) {
    return this.emailService.sendEmail(body, ip) || {};
  }
}