export interface ISendForm extends Record<any, any> {
  name?: string;
  email?: string;
  endpoint: string;
}
