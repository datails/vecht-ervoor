import { Module, HttpModule } from "@nestjs/common";
import { AppController } from "../../controllers/email-controller";
import { AppEmailService } from "../../services/email-service";

@Module({
  imports: [HttpModule],
  providers: [AppEmailService],
  controllers: [AppController],
})
export class EmailModule {}
