!(function () {
  "use strict";
  var h =
    "undefined" != typeof globalThis
      ? globalThis
      : "undefined" != typeof window
      ? window
      : "undefined" != typeof global
      ? global
      : "undefined" != typeof self
      ? self
      : {};
  function e(e) {
    return e &&
      e.__esModule &&
      Object.prototype.hasOwnProperty.call(e, "default")
      ? e.default
      : e;
  }
  function t(e, t, r) {
    return (
      e(
        (r = {
          path: t,
          exports: {},
          require: function (e, t) {
            return (function () {
              throw new Error(
                "Dynamic requires are not currently supported by @rollup/plugin-commonjs"
              );
            })(null == t && r.path);
          },
        }),
        r.exports
      ),
      r.exports
    );
  }
  var s = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function () {
          return (
            ("object" == typeof self && self.self === self && self) ||
            ("object" == typeof h && h.global === h && h) ||
            this
          );
        });
    }),
    k = t(function (e) {
      function t() {
        return (
          (e.exports = t =
            Object.assign ||
            function (e) {
              for (var t = 1; t < arguments.length; t++) {
                var r = arguments[t];
                for (var n in r)
                  Object.prototype.hasOwnProperty.call(r, n) && (e[n] = r[n]);
              }
              return e;
            }),
          t.apply(this, arguments)
        );
      }
      e.exports = t;
    }),
    r = t(function (e, t) {
      var u =
        (h && h.__spreadArrays) ||
        function () {
          for (var e = 0, t = 0, r = arguments.length; t < r; t++)
            e += arguments[t].length;
          var n = Array(e),
            a = 0;
          for (t = 0; t < r; t++)
            for (var i = arguments[t], o = 0, s = i.length; o < s; o++, a++)
              n[a] = i[o];
          return n;
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var d = [
        "userid",
        "user_id",
        "login",
        "username",
        "mail",
        "email",
        "e-mail",
        "user",
        "password",
        "iban",
      ];
      t.default = function (e) {
        var t = e.url,
          r = e.trailingSlash,
          n = void 0 === r || r,
          a = e.addFilterParams;
        if ("" === t || void 0 === t) return t;
        var i = u(d, void 0 === a ? [] : a),
          o = new RegExp(
            "\\.[a-z]*(html|pdf|jpg|png|aspx|asp|css|js|php)",
            "i"
          ),
          s = new RegExp("([?&#](" + i.join("|") + ")=)([^&#]*)", "ig");
        if (n && !o.test(t)) {
          var l = new RegExp(/\/?(\?|#|$)/);
          t = t.replace(l, "/$1");
        }
        return (t = t
          .replace(s, "$1removed")
          .replace(/\?&/, "?")
          .replace(/[?|&]$/, "")).trim();
      };
    }),
    n = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          if (void 0 !== e) return JSON.parse(JSON.stringify(e));
        });
    }),
    a = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e, t) {
          var n = e.split(".");
          return n.reduce(function (e, t, r) {
            return r === n.length - 1
              ? (delete e[t], !0)
              : e[t] === Object(e[t])
              ? e[t]
              : (e[t] = {});
          }, t);
        });
    }),
    i = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var n = r(s);
      t.default = function () {
        return n.default().window;
      };
    }),
    o = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var n = r(i);
      t.default = function () {
        return n.default().document;
      };
    }),
    l = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var s = r(o);
      t.default = function (e) {
        try {
          for (
            var t = s.default().cookie ? s.default().cookie.split("; ") : [],
              r = /(%[0-9A-Z]{2})+/g,
              n = t.length;
            0 < n--;

          ) {
            var a = t[n].split("="),
              i = a.slice(1).join("=");
            '"' === i.charAt(0) && (i = i.slice(1, -1));
            try {
              var o = a[0].replace(r, decodeURIComponent);
              if (((i = i.replace(r, decodeURIComponent)), e === o)) return i;
            } catch (e) {}
          }
        } catch (e) {
          return;
        }
      };
    }),
    u = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          return "[object Object]" === Object.prototype.toString.call(e) && !!e;
        });
    }),
    d = t(function (e, t) {
      var r =
          (h && h.__spreadArrays) ||
          function () {
            for (var e = 0, t = 0, r = arguments.length; t < r; t++)
              e += arguments[t].length;
            var n = Array(e),
              a = 0;
            for (t = 0; t < r; t++)
              for (var i = arguments[t], o = 0, s = i.length; o < s; o++, a++)
                n[a] = i[o];
            return n;
          },
        n =
          (h && h.__importDefault) ||
          function (e) {
            return e && e.__esModule ? e : { default: e };
          };
      Object.defineProperty(t, "__esModule", { value: !0 });
      function o(e, t, r, n) {
        for (var a in ((r = Array.isArray(r) ? r : []),
        (n = Array.isArray(n) ? n : []),
        t))
          if (s.default(t[a]) && t[a]) {
            var i = (null == e ? void 0 : e[a]) || void 0;
            void 0 === i && r.push(n.concat([a]).join(".")),
              o(i, t[a], r, n.concat([a]));
          } else
            (s.default(e) &&
              e &&
              JSON.stringify(e[a]) === JSON.stringify(t[a])) ||
              r.push(n.concat([a]).join("."));
        return r;
      }
      var s = n(u);
      t.default = function (e, t) {
        return r(o(e, t), o(t, e));
      };
    }),
    f = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e, t) {
          var r = t.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]"),
            n = RegExp("[\\?&]" + r + "=([^&#]*)").exec(e);
          return null === n ? "" : n[1];
        });
    }),
    c = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e, t) {
          return "" === e
            ? t
            : e.split(".").reduce(function (e, t) {
                return void 0 !== e ? e[t] : void 0;
              }, t);
        });
    }),
    v = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var i = r(o);
      t.default = function () {
        var e,
          t,
          r,
          n =
            null !==
              (r =
                null ===
                  (t =
                    null === (e = i.default()) || void 0 === e
                      ? void 0
                      : e.location) || void 0 === t
                  ? void 0
                  : t.hostname) && void 0 !== r
              ? r
              : "",
          a = null == n ? void 0 : n.split(".");
        return a.length <= 2
          ? n
          : Array.isArray(a)
          ? 2 === a[a.length - 1].length && 2 === a[a.length - 2].length
            ? a[a.length - 3] + "." + a[a.length - 2] + "." + a[a.length - 1]
            : a[a.length - 2] + "." + a[a.length - 1]
          : n;
      };
    }),
    p = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var i = r(o);
      t.default = function (e) {
        for (
          var t = /([^&=]+)=([^&]*)/g,
            r = {},
            n = i.default().location.search.substring(1),
            a = null;
          (a = t.exec(n));

        )
          r[decodeURIComponent(a[1])] = decodeURIComponent(a[2]);
        return r;
      };
    }),
    y = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var a = r(o);
      t.default = function (n) {
        return new Promise(function (e, t) {
          var r = new Image(0, 0);
          (r.onload = function () {
            return e();
          }),
            (r.onerror = function () {
              return t();
            }),
            a.default().body.appendChild(r),
            (r.src = n),
            (r.style.display = "none");
        });
      };
    }),
    g = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var a = r(o);
      t.default = function (n) {
        return new Promise(function (e, t) {
          var r = a.default().createElement("script");
          (r.onload = function () {
            return e();
          }),
            (r.onerror = function () {
              return t();
            }),
            a.default().head.appendChild(r),
            (r.type = "text/javascript"),
            (r.src = n);
        });
      };
    }),
    m = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          return Array.isArray(e);
        });
    }),
    _ = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          return "number" == typeof e && isFinite(e) && Math.floor(e) === e;
        });
    }),
    b = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          return "string" == typeof e || e instanceof String;
        });
    }),
    O = t(function (e, t) {
      var n =
        (h && h.__spreadArrays) ||
        function () {
          for (var e = 0, t = 0, r = arguments.length; t < r; t++)
            e += arguments[t].length;
          var n = Array(e),
            a = 0;
          for (t = 0; t < r; t++)
            for (var i = arguments[t], o = 0, s = i.length; o < s; o++, a++)
              n[a] = i[o];
          return n;
        };
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          for (var t = [], r = 1; r < arguments.length; r++)
            t[r - 1] = arguments[r];
          return Object.assign.apply(Object, n([e], t));
        });
    }),
    P = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var f = r(o),
        c = r(v),
        p = r(_);
      t.default = function (e) {
        var t = e.name,
          r = e.value,
          n = e.days,
          a = void 0 === n ? 30 : n,
          i = e.path,
          o = void 0 === i ? "/" : i,
          s = e.domain,
          l = void 0 === s ? c.default() : s;
        try {
          var u = "";
          if (p.default(a)) {
            var d = new Date();
            d.setMilliseconds(d.getMilliseconds() + 864e5 * a),
              (u = "; expires=" + d.toUTCString());
          }
          (r = encodeURIComponent(String(r)).replace(
            /%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,
            decodeURIComponent
          )),
            (f.default().cookie =
              t + "=" + r + u + ";domain=" + l + ";path=" + o);
        } catch (e) {
          console.error(e);
        }
      };
    }),
    j = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var i = r(u),
        o = r(O);
      t.default = function (e, a, t) {
        return e.split(".").reduce(function (e, t, r, n) {
          return r === n.length - 1
            ? i.default(e[t]) && i.default(a)
              ? o.default(e[t], a)
              : (e[t] = a)
            : i.default(e[t])
            ? e[t]
            : (e[t] = {});
        }, t);
      };
    }),
    A = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function () {
          return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
            /[xy]/g,
            function (e) {
              var t = (16 * Math.random()) | 0;
              return ("x" == e ? t : (3 & t) | 8).toString(16);
            }
          );
        });
    }),
    S = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 });
      var r = /(klant(?:nr|nummer)(?:.*(?:\s|\:|\||\.))?)([0-9]{4,15})((?:[a-z]|\-|$|\s|\||\.).*)/gim,
        n = /(^|.*(?:\s|\:|\||\.)+)((?:(?:\+|00(?:\s|\s?\-\s?)?)[0-9][0-9](?:\s|\s?\-\s?)?(?:\(0\)[\-\s]?)?|0)[1-9](?:(?:\s|\s?\-\s?)?[0-9])(?:(?:\s|\s?-\s?)?[0-9])(?:(?:\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9])((?:$|[a-z]|\-|\s|\|).*)/gim,
        a = /(^|.*(?:\s|\:|\||\.)+)([1-9][0-9]{3} ?[a-z]{2})((?:$|\-|\s|\||\-).*)/gim,
        i = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))(.*$)/gim,
        o = /(^|.*(?:\s|\:|\||\.)+)([A-Z]{2}\s?[0-9]{2}\s?[A-Z]{4}\s?[0-9]{3,4}\s?[0-9]{2,6}\s?[0-9]{1,6})((?:[a-z]|\-|$|\s|\||\.).*)/gim;
      t.default = {
        hasCustomerId: function (e) {
          return r.test(e);
        },
        hasEmailAddress: function (e) {
          return i.test(e);
        },
        hasIBAN: function (e) {
          return o.test(e);
        },
        hasPhoneNumber: function (e) {
          return n.test(e);
        },
        hasZipCode: function (e) {
          return a.test(e);
        },
        replaceCustomerId: function (e) {
          return e.replace(r, "$1[MASKED_CUSTOMER_ID]$3");
        },
        replaceEmailAddress: function (e) {
          return e.replace(i, "[MASKED_EMAIL]");
        },
        replaceIBAN: function (e) {
          return e.replace(o, "$1[MASKED_IBAN]$3");
        },
        replacePhoneNumber: function (e) {
          return e.replace(n, "$1[MASKED_PHONENR]$3");
        },
        replaceZipCode: function (e) {
          return e.replace(a, "$1[MASKED_ZIPCODE]$3");
        },
      };
    }),
    D = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.cleanUrl = r.default),
        (t.cloneObject = n.default),
        (t.deletePropAtPath = a.default),
        (t.getCookie = l.default),
        (t.getDifferences = d.default),
        (t.getDocument = o.default),
        (t.getGlobal = s.default),
        (t.getParam = f.default),
        (t.getPropAtPath = c.default),
        (t.getRootDomain = v.default),
        (t.getSearchParams = p.default),
        (t.getWindow = i.default),
        (t.injectPixel = y.default),
        (t.injectScript = g.default),
        (t.isArray = m.default),
        (t.isInteger = _.default),
        (t.isObject = u.default),
        (t.isString = b.default),
        (t.mergeObjects = O.default),
        (t.setCookie = P.default),
        (t.setPropAtPath = j.default),
        (t.createUuid = A.default),
        (t.regex = S.default);
    }),
    M = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      function o(e) {
        var t = {};
        for (var r in (void 0 !== e.arguments &&
          ((e.arguments = JSON.parse(e.arguments)),
          console.error(
            "An error was caught in a script using a DDM functionality:\n                function: " +
              e.function +
              "\n                id: " +
              e.id +
              "\n                event: " +
              JSON.stringify(e.arguments) +
              "\n                message: " +
              e.message +
              "\n                stack: " +
              e.stack +
              "\n            "
          )),
        e))
          e[r] instanceof Object &&
            "arguments" === r &&
            (e[r] = JSON.stringify(e[r]).replace(/(\"|")/g, "")),
            void 0 !== e[r] &&
              (t[r] = e[r]
                .replace(/\r?\n|\r/g, "")
                .replace(/[ ]+/g, " ")
                .slice(0, 255));
        return (
          (function (e) {
            var t, r, n, a;
            null ===
              (a =
                null ===
                  (r =
                    null === (t = i.default()) || void 0 === t
                      ? void 0
                      : t._ddm) || void 0 === r
                  ? void 0
                  : (n = r).trigger) ||
              void 0 === a ||
              a.call(n, "_ddm.error", { data: k({}, e) });
          })(e),
          !1
        );
      }
      var i = r(s);
      t.default = function (e) {
        var t,
          r = e.error,
          n = e.name,
          a = e.args,
          i = e.id;
        try {
          t = JSON.stringify(a);
        } catch (e) {
          t = "error during serialization of arguments: " + e.message;
        }
        return o({
          arguments: t,
          function: n,
          id: i,
          message: r.message ? r.message : "",
          stack: r.stack ? r.stack : "",
        });
      };
    }),
    w = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var i = r(M);
      t.default = function (e, n, t) {
        var a = t.value;
        return (
          (t.value = function () {
            for (var e = arguments.length, t = new Array(e), r = 0; r < e; r++)
              t[r] = arguments[r];
            try {
              return a.apply(this, t);
            } catch (e) {
              i.default({
                error: e,
                name: n,
                args: t,
                id: "DDM Error: internal ddm error: _ddm." + n,
              });
            }
          }),
          t
        );
      };
    }),
    x = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e, t) {
          return -1 !== e.indexOf(t);
        });
    }),
    L = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e, s) {
          var l = s.hasOwnProperty("**")
            ? [{ handlers: s["**"], path: "**" }]
            : [];
          return (
            e.split(".").reduce(function (e, t, r, n) {
              if (r === n.length - 1) {
                var a = e.concat(["*"]).join("."),
                  i = e.concat([t]).join(".");
                l = [].concat(
                  l,
                  s.hasOwnProperty(a) ? [{ path: a, handlers: s[a] }] : [],
                  s.hasOwnProperty(i) ? [{ path: i, handlers: s[i] }] : []
                );
              } else {
                var o = e.concat([t, "**"]).join(".");
                l = [].concat(
                  l,
                  s.hasOwnProperty(o) ? [{ path: o, handlers: s[o] }] : []
                );
              }
              return e.push(t), e;
            }, []),
            l.reverse(),
            l
          );
        });
    }),
    C = t(function (e, t) {
      var r;
      Object.defineProperty(t, "__esModule", { value: !0 }),
        ((r = t.PersistenceKeys || (t.PersistenceKeys = {})).listeners =
          "ddm:root:persistedListeners"),
        (r.paths = "ddm:root:persistedPaths"),
        (r.time = "ddm:root:persistedTime");
    }),
    E = t(function (e, t) {
      var m = arguments,
        r =
          (h && h.__importStar) ||
          function (e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
              for (var r in e)
                Object.hasOwnProperty.call(e, r) && (t[r] = e[r]);
            return (t.default = e), t;
          },
        n =
          (h && h.__importDefault) ||
          function (e) {
            return e && e.__esModule ? e : { default: e };
          };
      Object.defineProperty(t, "__esModule", { value: !0 });
      function a(e) {
        var t = e.listenerSet,
          f = e.props,
          r = _.getDifferences(
            JSON.parse(JSON.stringify(f.history)),
            JSON.parse(JSON.stringify(f.datalayer))
          );
        r = (r = r.concat(
          (function (e) {
            var t = Object.entries(e),
              r = [];
            return (
              t.forEach(function (e) {
                e[1].filter(function (e) {
                  return !1 === e.onlyInvokeOnChanges;
                }).length && r.push(e[0]);
              }),
              r
            );
          })(t)
        ))
          .filter(function (e, t) {
            return r.indexOf(e) === t;
          })
          .reverse();
        var c = {},
          p = [],
          n = r,
          a = Array.isArray(n),
          i = 0;
        for (n = a ? n : n[Symbol.iterator](); ; ) {
          var o;
          if (a) {
            if (i >= n.length) break;
            o = n[i++];
          } else {
            if ((i = n.next()).done) break;
            o = i.value;
          }
          var s = o,
            l = function () {
              if (v) {
                if (y >= h.length) return "break";
                g = h[y++];
              } else {
                if ((y = h.next()).done) return "break";
                g = y.value;
              }
              var e = g,
                t = e.handlers,
                r = e.path;
              if (!c.hasOwnProperty(r)) {
                var n = t,
                  a = Array.isArray(n),
                  i = 0;
                for (n = a ? n : n[Symbol.iterator](); ; ) {
                  var o;
                  if (a) {
                    if (i >= n.length) break;
                    o = n[i++];
                  } else {
                    if ((i = n.next()).done) break;
                    o = i.value;
                  }
                  var s = o,
                    l = s.handler,
                    u = s.id,
                    d = s.once;
                  "string" == typeof l &&
                    (l = function () {
                      var e;
                      _.setPropAtPath(
                        r.replace(/\.?\*\*?$/, ""),
                        null !=
                          (e = _.getPropAtPath(
                            r.replace(/\.?\*\*?$/, ""),
                            k({}, f.datalayer)
                          ))
                          ? e
                          : "not_set",
                        f.persistedPaths
                      ),
                        _.getGlobal().localStorage
                          ? _.getGlobal().localStorage.setItem(
                              O.PersistenceKeys.paths,
                              JSON.stringify(f.persistedPaths)
                            )
                          : _.setCookie({
                              name: O.PersistenceKeys.paths,
                              value: JSON.stringify(f.persistedPaths),
                            });
                    });
                  try {
                    d ||
                      !0 !==
                        l(
                          _.getPropAtPath(
                            r.replace(/\.?\*\*?$/, ""),
                            k({}, f.datalayer)
                          )
                        ) ||
                      (d = !0);
                  } catch (e) {
                    b.default({ args: m, error: e, id: u, name: l.toString() });
                  }
                  p.push(l);
                }
                c[r] = 1;
              }
            },
            h = u.default(s, t),
            v = Array.isArray(h),
            y = 0;
          for (h = v ? h : h[Symbol.iterator](); ; ) {
            var g;
            if ("break" === l()) break;
          }
        }
        return p;
      }
      var _ = r(D),
        u = n(L),
        b = n(M),
        O = r(C);
      t.default = function (e) {
        var t = a({ listenerSet: e.persistedListeners, props: e }),
          r = a({ listenerSet: e.changeListeners, props: e });
        return [].concat(t, r);
      };
    }),
    I = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          var t,
            r,
            n = e.path,
            a = e.handler,
            i = e.listeners,
            o = e.dependsOn,
            s = e.id,
            l = e.onlyInvokeOnChanges,
            u =
              null != (r = null === (t = i) || void 0 === t ? void 0 : t[n])
                ? r
                : [];
          return (
            u.push({ handler: a, id: s, dependsOn: o, onlyInvokeOnChanges: l }),
            (i[n] = u),
            !0
          );
        });
    }),
    N = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          for (
            var n = e.handlerObject,
              t = e.event,
              a = e.events,
              r = [].concat(a),
              i = r.slice(),
              o = r.length - 1;
            0 <= o;
            o--
          ) {
            if (r[o].name === "dependsOn_" + n.dependsOn.join(".")) {
              i = r.slice(o);
              break;
            }
          }
          for (
            var s = 0,
              l = {},
              u = function (r) {
                (n.dependsOn[r] !== t.name &&
                  !i.some(function (e) {
                    return e.name === n.dependsOn[r];
                  })) ||
                  ((s += 1),
                  (i = null != i ? i : a).filter(function (e) {
                    var t;
                    e.name === n.dependsOn[r] &&
                      (l = k(
                        {},
                        l,
                        (((t = {})[e.name] = l[e.name]
                          ? k({}, l[e.name], e)
                          : e),
                        t)
                      ));
                  }));
              },
              d = n.dependsOn.length;
            0 < d--;

          )
            u(d);
          return { mergedEventObjects: l, numTriggered: s };
        });
    }),
    J = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.default = function (e) {
          var t,
            r,
            n,
            a = e.listeners,
            i = e.name,
            o = e.handler,
            s = e.id,
            l =
              null != (r = null === (t = a) || void 0 === t ? void 0 : t[i])
                ? r
                : [];
          for (var u in l) {
            var d;
            if (
              l[u].handler === o ||
              (null === (n = l[u]) || void 0 === n ? void 0 : n.id) === s ||
              (!l[u].handler.hasOwnProperty("prototype") &&
                l[u].handler.toString() === o.toString()) ||
              l[u].handler.name === o.name
            )
              l.splice(parseInt(u, 10), 1),
                (a = k({}, a, (((d = {})[i] = l), d)));
          }
        });
    }),
    R = t(function (e, t) {
      var r =
        (h && h.__importDefault) ||
        function (e) {
          return e && e.__esModule ? e : { default: e };
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var i = r(n);
      t.default = function (t, r) {
        var n = Date.now(),
          e = i.default(t.persistedPaths),
          a = i.default(t.persistedPaths);
        return (
          Object.keys(t.persistedTime).forEach(function (e) {
            t.persistedTime[e] < n && r.unpersist(e);
          }),
          k({}, t, { datalayer: e, history: a })
        );
      };
    }),
    $ = t(function (e, t) {
      var r =
        (h && h.__importStar) ||
        function (e) {
          if (e && e.__esModule) return e;
          var t = {};
          if (null != e)
            for (var r in e) Object.hasOwnProperty.call(e, r) && (t[r] = e[r]);
          return (t.default = e), t;
        };
      Object.defineProperty(t, "__esModule", { value: !0 });
      var n = r(C);
      t.default = function (e) {
        var t = D.getGlobal();
        void 0 !== t.localStorage &&
          (t.localStorage.setItem(
            n.PersistenceKeys.paths,
            JSON.stringify(e.persistedPaths)
          ),
          t.localStorage.setItem(
            n.PersistenceKeys.time,
            JSON.stringify(e.persistedTime)
          ),
          t.localStorage.setItem(
            n.PersistenceKeys.listeners,
            JSON.stringify(e.persistedListeners)
          ));
      };
    }),
    T = t(function (e, t) {
      Object.defineProperty(t, "__esModule", { value: !0 }),
        (t.arrayHasValue = x.default),
        (t.getListeners = L.default),
        (t.invokeListeners = E.default),
        (t.registerListener = I.default),
        (t.mergeTriggerObjects = N.default),
        (t.removeListener = J.default),
        (t.handleError = M.default),
        (t.restoreDataLayer = R.default),
        (t.setPersistency = $.default);
    }),
    G = t(function (e, t) {
      var r =
          (h && h.__decorate) ||
          function (e, t, r, n) {
            var a,
              i = arguments.length,
              o =
                i < 3
                  ? t
                  : null === n
                  ? (n = Object.getOwnPropertyDescriptor(t, r))
                  : n;
            if (
              "object" == typeof Reflect &&
              "function" == typeof Reflect.decorate
            )
              o = Reflect.decorate(e, t, r, n);
            else
              for (var s = e.length - 1; 0 <= s; s--)
                (a = e[s]) &&
                  (o = (i < 3 ? a(o) : 3 < i ? a(t, r, o) : a(t, r)) || o);
            return 3 < i && o && Object.defineProperty(t, r, o), o;
          },
        n =
          (h && h.__importStar) ||
          function (e) {
            if (e && e.__esModule) return e;
            var t = {};
            if (null != e)
              for (var r in e)
                Object.hasOwnProperty.call(e, r) && (t[r] = e[r]);
            return (t.default = e), t;
          },
        a =
          (h && h.__importDefault) ||
          function (e) {
            return e && e.__esModule ? e : { default: e };
          };
      Object.defineProperty(t, "__esModule", { value: !0 });
      function i(e) {
        return M.getGlobal().localStorage
          ? JSON.parse(
              M.getGlobal().localStorage.getItem(e) || JSON.stringify({})
            )
          : {};
      }
      var M = n(D),
        o = a(w),
        x = n(T),
        s = n(C),
        l = (function () {
          function e(d) {
            var f = this;
            void 0 === d && (d = M.getGlobal()._ddm || {}),
              (this.utils = k({}, M.regex, {
                createRandom: M.createUuid,
                deletePropAtPath: M.deletePropAtPath,
                getDifferences: M.getDifferences,
                getParam: M.getParam,
                getPropAtPath: M.getPropAtPath,
                getRootDomain: M.getRootDomain,
                injectPixel: M.injectPixel,
                injectScript: M.injectScript,
                setPropAtPath: M.setPropAtPath,
              })),
              (this.cleanUrl = M.cleanUrl),
              (this.isArray = M.isArray),
              (this.isInteger = M.isInteger),
              (this.isObject = M.isObject),
              (this.isString = M.isString),
              (this.cloneObject = M.cloneObject),
              (this.mergeObjects = M.mergeObjects),
              (this.setCookie = M.setCookie),
              (this.getCookie = M.getCookie),
              (this.props = {
                changeListeners: {},
                datalayer: {},
                eventListeners: {},
                events: [],
                history: {},
                persistedListeners: i(s.PersistenceKeys.listeners),
                persistedPaths: i(s.PersistenceKeys.paths),
                persistedTime: i(s.PersistenceKeys.time),
              }),
              (this.events = this.props.events),
              (M.getGlobal().dataLayer = M.getGlobal().dataLayer || []),
              (this.props = x.restoreDataLayer(this.props, this)),
              this.listen("_ddm.core.restored", function () {
                if (M.isObject(d) && M.isArray(d.l) && M.isArray(d.e)) {
                  var e = d.l,
                    t = Array.isArray(e),
                    r = 0;
                  for (e = t ? e : e[Symbol.iterator](); ; ) {
                    var n;
                    if (t) {
                      if (r >= e.length) break;
                      n = e[r++];
                    } else {
                      if ((r = e.next()).done) break;
                      n = r.value;
                    }
                    var a = n;
                    f.listen.apply(f, a);
                  }
                  var i = d.e,
                    o = Array.isArray(i),
                    s = 0;
                  for (i = o ? i : i[Symbol.iterator](); ; ) {
                    var l;
                    if (o) {
                      if (s >= i.length) break;
                      l = i[s++];
                    } else {
                      if ((s = i.next()).done) break;
                      l = s.value;
                    }
                    var u = l;
                    f.trigger.apply(f, u);
                  }
                }
              }),
              Object.freeze(this);
          }
          var t = e.prototype;
          return (
            (t.trigger = function (e, t) {
              var i = this;
              void 0 === t && (t = {});
              for (
                var o = k(
                    { event: e, name: e, timestamp: new Date().getTime() },
                    M.cloneObject(t)
                  ),
                  r = 0,
                  n = [this.props.events, M.getGlobal().dataLayer];
                r < n.length;
                r++
              ) {
                n[r].push(M.cloneObject(o));
              }
              var s = [];
              if (o.hasOwnProperty("dd") && M.isObject(o.dd)) {
                var a = (function (e, t) {
                    var r = M.cloneObject(e.datalayer);
                    return {
                      datalayer: k({}, e.datalayer, M.cloneObject(t)),
                      history: r,
                    };
                  })(this.props, o.dd),
                  l = a.datalayer,
                  u = a.history;
                (this.props.history = k({}, this.props.history, u)),
                  (this.props.datalayer = k({}, this.props.datalayer, l)),
                  (s = x.invokeListeners(this.props));
              }
              var d,
                f = x.getListeners(e, this.props.eventListeners),
                c = Array.isArray(f),
                p = 0;
              for (f = c ? f : f[Symbol.iterator](); ; ) {
                var h;
                if (c) {
                  if (p >= f.length) break;
                  h = f[p++];
                } else {
                  if ((p = f.next()).done) break;
                  h = p.value;
                }
                h.handlers.forEach(function (t) {
                  var e;
                  if (
                    null === (e = t) || void 0 === e
                      ? void 0
                      : e.hasOwnProperty("handler")
                  )
                    if (
                      t.hasOwnProperty("dependsOn") &&
                      M.isArray(t.dependsOn)
                    ) {
                      var r = x.mergeTriggerObjects({
                          event: o,
                          events: i.events,
                          handlerObject: t,
                        }),
                        n = r.numTriggered,
                        a = r.mergedEventObjects;
                      if (
                        n === t.dependsOn.length &&
                        !x.arrayHasValue(s, t.handler)
                      ) {
                        d = {
                          items: t.dependsOn,
                          name: "dependsOn_" + t.dependsOn.join("."),
                          timestamp: new Date().getTime(),
                        };
                        try {
                          t.hasOwnProperty("once") ||
                            !0 !== t.handler(a) ||
                            (t.once = !0);
                        } catch (e) {
                          x.handleError({
                            args: a,
                            error: e,
                            id: t.id,
                            name: t.handler.toString(),
                          });
                        }
                      }
                    } else if (!x.arrayHasValue(s, t.handler))
                      try {
                        t.hasOwnProperty("once") ||
                          !0 !== t.handler(o) ||
                          (t.once = !0);
                      } catch (e) {
                        x.handleError({
                          args: o,
                          error: e,
                          id: t.id,
                          name: t.handler.toString(),
                        });
                      }
                });
              }
              if (void 0 !== d) {
                var v = M.cloneObject(d);
                this.props.events.push(v),
                  M.getGlobal().dataLayer.push(
                    k({ event: v.name }, M.cloneObject(v))
                  );
              }
            }),
            (t.isTriggered = function (t, e) {
              return (
                void 0 === e && (e = [].concat(this.props.events)),
                e.some(function (e) {
                  return e.name === t;
                })
              );
            }),
            (t.listen = function (e, t, r, n) {
              var a,
                l = this;
              if (
                (M.isString(r) && ((n = r), "function" == typeof t && (r = t)),
                void 0 === r && "function" == typeof t && (r = t),
                Array.isArray(e))
              ) {
                var i = e,
                  o = Array.isArray(i),
                  s = 0;
                for (i = o ? i : i[Symbol.iterator](); ; ) {
                  var u;
                  if (o) {
                    if (s >= i.length) break;
                    u = i[s++];
                  } else {
                    if ((s = i.next()).done) break;
                    u = s.value;
                  }
                  var d = u;
                  x.registerListener({
                    dependsOn: e,
                    handler: r,
                    id: n,
                    listeners: this.props.eventListeners,
                    path: d,
                  });
                }
              } else
                x.registerListener({
                  handler: r,
                  id: n,
                  listeners: this.props.eventListeners,
                  path: e,
                });
              if (
                "boolean" != typeof t ||
                ("boolean" == typeof t && !1 !== t)
              ) {
                var f = {},
                  c = this.props.events,
                  p = Array.isArray(c),
                  h = 0;
                for (c = p ? c : c[Symbol.iterator](); ; ) {
                  var v;
                  if (p) {
                    if (h >= c.length) break;
                    v = c[h++];
                  } else {
                    if ((h = c.next()).done) break;
                    v = h.value;
                  }
                  var y = v;
                  f[y.name] = !0;
                  var g = x.getListeners(y.name, this.props.eventListeners),
                    m = Array.isArray(g),
                    _ = 0;
                  for (g = m ? g : g[Symbol.iterator](); ; ) {
                    var b;
                    if (m) {
                      if (_ >= g.length) break;
                      b = g[_++];
                    } else {
                      if ((_ = g.next()).done) break;
                      b = _.value;
                    }
                    var O = b.handlers,
                      P = Array.isArray(O),
                      j = 0;
                    for (O = P ? O : O[Symbol.iterator](); ; ) {
                      var A;
                      if (P) {
                        if (j >= O.length) break;
                        A = O[j++];
                      } else {
                        if ((j = O.next()).done) break;
                        A = j.value;
                      }
                      var S = A;
                      if (
                        null === (a = S) || void 0 === a
                          ? void 0
                          : a.hasOwnProperty("handler")
                      )
                        if (
                          S.hasOwnProperty("dependsOn") &&
                          Array.isArray(S.dependsOn)
                        )
                          !(function () {
                            var e = 0,
                              n = {},
                              t = function () {
                                if (i) {
                                  if (o >= a.length) return "break";
                                  s = a[o++];
                                } else {
                                  if ((o = a.next()).done) return "break";
                                  s = o.value;
                                }
                                var r = s;
                                (e += f.hasOwnProperty(r) ? 1 : 0),
                                  l.props.events.filter(function (e) {
                                    var t;
                                    e.name === r &&
                                      (n = k(
                                        {},
                                        n,
                                        (((t = {})[e.name] = n[e.name]
                                          ? k({}, n[e.name], e)
                                          : e),
                                        t)
                                      ));
                                  });
                              },
                              a = S.dependsOn,
                              i = Array.isArray(a),
                              o = 0;
                            for (a = i ? a : a[Symbol.iterator](); ; ) {
                              var s;
                              if ("break" === t()) break;
                            }
                            if (e === S.dependsOn.length && r === S.handler)
                              try {
                                S.hasOwnProperty("once") ||
                                  !0 !== S.handler(n) ||
                                  (S.once = !0);
                              } catch (e) {
                                x.handleError({
                                  args: y,
                                  error: e,
                                  id: S.id,
                                  name: S.handler.toString(),
                                });
                              }
                          })();
                        else if (r === S.handler)
                          try {
                            S.hasOwnProperty("once") ||
                              !0 !== S.handler(y) ||
                              (S.once = !0);
                          } catch (e) {
                            x.handleError({
                              args: y,
                              error: e,
                              id: S.id,
                              name: S.handler.toString(),
                            });
                          }
                    }
                  }
                }
              }
            }),
            (t.unlisten = function (e, t, r) {
              x.removeListener({
                handler: t,
                id: r,
                listeners: this.props.eventListeners,
                name: e,
              });
            }),
            (t.change = function (e, t, r, i) {
              var n,
                o = this,
                s = [e + ".**", e];
              M.isString(r) && ((i = r), (r = t)),
                void 0 === r && ((n = !1), (r = t));
              var l = r;
              (!1 !== n && !1 !== t) || (s = [e]);
              for (var a = 0, u = s; a < u.length; a++) {
                var d = u[a];
                x.registerListener({
                  handler: l,
                  id: i,
                  listeners: this.props.changeListeners,
                  onlyInvokeOnChanges: n || t,
                  path: d,
                });
              }
              if ("function" == typeof t || !0 !== t) {
                var f = M.getPropAtPath(
                  e.replace(/\.?\*\*?$/, ""),
                  this.props.datalayer
                );
                void 0 !== f && r(f);
              }
              return {
                unlisten: function () {
                  var e = s,
                    t = Array.isArray(e),
                    r = 0;
                  for (e = t ? e : e[Symbol.iterator](); ; ) {
                    var n;
                    if (t) {
                      if (r >= e.length) break;
                      n = e[r++];
                    } else {
                      if ((r = e.next()).done) break;
                      n = r.value;
                    }
                    var a = n;
                    x.removeListener({
                      handler: l,
                      id: i,
                      listeners: o.props.changeListeners,
                      name: a,
                    });
                  }
                },
              };
            }),
            (t.empty = function (e) {
              var t = M.getPropAtPath(e, this.props.datalayer);
              return (
                null == t ||
                void 0 === t ||
                (M.isObject(t)
                  ? JSON.stringify(t) === JSON.stringify({})
                  : M.isArray(t)
                  ? 0 === t.length
                  : M.isString(t)
                  ? "" === t.trim()
                  : !M.isInteger(t))
              );
            }),
            (t.erase = function (e) {
              this.has(e) &&
                ((this.props.history = M.cloneObject(this.props.datalayer)),
                M.deletePropAtPath(e, this.props.datalayer),
                this.unpersist(e),
                (M.getGlobal()._dd = M.cloneObject(this.props.datalayer)),
                x.invokeListeners(this.props));
            }),
            (t.set = function (e, t, r, n) {
              return (
                void 0 === r && (r = !1),
                void 0 === n && (n = 30),
                (this.props.history = M.cloneObject(this.props.datalayer)),
                r && this.persist(e, M.isInteger(n) ? n : 30),
                M.setPropAtPath(e, t, this.props.datalayer),
                (M.getGlobal()._dd = M.cloneObject(this.props.datalayer)),
                x.invokeListeners(this.props),
                t
              );
            }),
            (t.push = function (e, t) {
              this.props.history = M.cloneObject(this.props.datalayer);
              var r = this.get(e);
              return (
                M.isArray(r)
                  ? M.setPropAtPath(
                      e,
                      r.concat(M.isArray(t) ? t : [M.cloneObject(t)]),
                      this.props.datalayer
                    )
                  : this.set(e, M.isArray(t) ? t : [M.cloneObject(t)]),
                x.invokeListeners(this.props),
                this.get(e)
              );
            }),
            (t.has = function (e) {
              return void 0 !== M.getPropAtPath(e, this.props.datalayer);
            }),
            (t.get = function (e, t, r) {
              if ((void 0 === r && (r = !1), void 0 === e))
                return M.cloneObject(this.props.datalayer);
              var n = M.getPropAtPath(e, this.props.datalayer);
              return M.cloneObject(
                void 0 === n && void 0 !== t
                  ? !0 === r
                    ? this.set(e, t)
                    : t
                  : n
              );
            }),
            (t.getDDMState = function () {
              return { props: this.props };
            }),
            (t.removeCookie = function (e, t, r) {
              return (
                void 0 === t && (t = "/"),
                void 0 === r && (r = M.getRootDomain()),
                M.setCookie({ domain: r, name: e, path: t, value: -100 })
              );
            }),
            (t.getRefParam = function (e) {
              return M.getParam(M.getGlobal().document.referrer, e);
            }),
            (t.getUrlParam = function (e) {
              return M.getParam(M.getGlobal().window.location.href, e);
            }),
            (t.persist = function (e, t) {
              var r;
              void 0 === t && (t = 30);
              for (var n = 0, a = [e, e + ".**"]; n < a.length; n++) {
                var i,
                  o,
                  s = a[n];
                if (!this.props.persistedPaths.hasOwnProperty(s))
                  this.props.persistedListeners = k(
                    {},
                    this.props.persistedListeners,
                    (((o = {})[s] = [
                      { handler: String(function () {}), path: s },
                    ]),
                    o)
                  );
                this.props.persistedTime = k(
                  {},
                  this.props.persistedTime,
                  (((i = {})[s] = new Date(Date.now() + 6e4 * t).getTime()), i)
                );
              }
              M.setPropAtPath(
                e,
                null != (r = this.get(e)) ? r : "not_set",
                this.props.persistedPaths
              ),
                x.setPersistency(this.props);
            }),
            (t.unpersist = function (e) {
              M.deletePropAtPath(e, this.props.persistedPaths),
                delete this.props.persistedListeners[e],
                delete this.props.persistedListeners[e + ".**"],
                delete this.props.persistedTime[e],
                x.setPersistency(this.props);
            }),
            e
          );
        })();
      r([o.default], l.prototype, "trigger", null),
        r([o.default], l.prototype, "listen", null),
        r([o.default], l.prototype, "change", null),
        r([o.default], l.prototype, "erase", null),
        r([o.default], l.prototype, "persist", null),
        r([o.default], l.prototype, "unpersist", null),
        (t.default = l);
    });
  t(function (e, t) {
    var r =
      (h && h.__importDefault) ||
      function (e) {
        return e && e.__esModule ? e : { default: e };
      };
    Object.defineProperty(t, "__esModule", { value: !0 });
    var n = r(s),
      a = r(G);
    n.default()._ddm = new a.default();
  });
})();
