import React from "react";
import AppHeader from "../../../../components/app-hero-header";

export default function ({ subTitle, title }) {
  return (
    <AppHeader subTitle={subTitle} title={title} background="sunset.jpg" />
  );
}
